async function getBidsData(cardId) {
  let response = await fetch(`/lots/api/bids/${cardId}/`, {
    headers: new Headers({
      "X-CSRFToken": getCookie("csrftoken"),
      "X-Requested-With": "XMLHttpRequest",
    }),
  });
  return response.json();
}

async function renderBids(cardId) {
  spinner.removeAttribute("hidden");
  let response = await getBidsData(cardId);
  messages.textContent = "";

  if (Object.keys(response).length === 0 && response.constructor === Object) {
    const noBids = document.createElement("span");
    noBids.setAttribute("class", "bidding__no_bids");
    noBids.innerHTML = `Стартовая цена: ${startPrice}`;
    messages.appendChild(noBids);
  } else {
    const timeline = document.createElement("div");
    timeline.className = "bidding__timeline";
    messages.appendChild(timeline);
  }

  for (const [key, value] of Object.entries(response)) {
    let [phone, price, step, date] = value;
    date = new Date(Date.parse(date));
    const newMessage =
      phone === userPhone
        ? createBidElement(price, step, date, "own")
        : createBidElement(price, step, date, "foreign");
    messages.appendChild(newMessage);
  }
  spinner.setAttribute("hidden", "");
  overlay.setAttribute("hidden", "");
}

function formatPrice(price) {
  return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
}

function formatDate(date) {
  const day = date.getDay()
  const monthNumber = date.getMonth()
  const monthMapping = {0: 'янв.', 1: 'фев.', 2: 'марта', 3: 'апр.', 4: 'мая', 5: 'июня', 6: 'июля', 7: 'авг.', 8: 'сент.', 9: 'окт.', 10: 'нояб.', 11: 'дек.'}
  console.log(typeof monthNumber)
  return `${day} ${monthMapping[monthNumber]}`
}

function createBidElement(price, step, date, type) {
  const messageWrapper = document.createElement("div");
  messageWrapper.classList.add("bidding__message");
  if (type === "foreign") {
    messageWrapper.classList.add("bidding__message-foreign")
  }
  if (type === "own") {
    messageWrapper.innerHTML = `
            <div class="message__indent"></div>
            <div class="message__datetime">
              <span class="message__date">${formatDate(date)}</span>
              <span class="message__time">${date.getHours()}:${date.getMinutes()}</span>
            </div>
            <div class="message__content message__content-right">
              <svg class="message__svg" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-8.707l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 001.414 1.414L9 9.414V13a1 1 0 102 0V9.414l1.293 1.293a1 1 0 001.414-1.414z" clip-rule="evenodd"></path>
              </svg>
              <span class="message__step">${step}</span>
              <span class="message__price">${formatPrice(price)}</span>
              <span class="message__percent">${formatPrice(price*1.05)}</span>
            </div>`;
  } else {
    messageWrapper.innerHTML = `
            <div class="message__indent"></div>
            <div class="message__datetime">
              <span class="message__date">${formatDate(date)}</span>
              <span class="message__time">${date.getHours()}:${date.getMinutes()}</span>
            </div>
            <div class="message__content message__content-left ">
              <svg class="message__svg" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-8.707l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 001.414 1.414L9 9.414V13a1 1 0 102 0V9.414l1.293 1.293a1 1 0 001.414-1.414z" clip-rule="evenodd"></path>
              </svg>
              <span class="message__step">${step}</span>
              <span class="message__price">${formatPrice(price)}</span>
              <span class="message__percent">${formatPrice(price*1.05)}</span>
            </div>`;
  }

  return messageWrapper;
}

const roomName = JSON.parse(document.getElementById("room-name").textContent);
const startPrice = JSON.parse(document.getElementById("start-price").textContent);
const cardId = JSON.parse(document.getElementById("card-id").textContent);
const userPhone = JSON.parse(document.getElementById("user-phone").textContent);
const userRole = JSON.parse(document.getElementById("user-role").textContent);

const messages = document.querySelector(".bidding__messages");
const spinner = document.querySelector(".spinner");
const overlay = document.querySelector(".overlay");
const alertContainer = document.querySelector(".bidding__alert");
const alertText = alertContainer.querySelector(".bidding__alert__text");
const bidSubmitBtn = document.querySelector(".bidding__ui__button");
const infoSection = document.querySelector(".bidding__sidebar__content");
const radioElements = document.querySelectorAll(".bidding__ui__input") 

function timer(expiry) {
  return {
    expiry: expiry,
    remaining: null,
    init() {
      this.setRemaining()
      setInterval(() => {
        this.setRemaining();
      }, 1000);
    },
    setRemaining() {
      const diff = this.expiry - new Date().getTime();
      this.remaining =  parseInt(diff / 1000);
    },
    days() {
      return {
        value: this.format(this.remaining / 3600 / 24),
        remaining: this.remaining % (3600 * 24)
      }
    },
    hours() {
      return {
        value: this.format(this.days().remaining / 3600),
        remaining: this.days().remaining % 3600
      };
    },
    minutes() {
      return {
        value: this.format(this.hours().remaining / 60),
        remaining: this.hours().remaining % 60
      };
    },
    seconds() {
      return {
        value: this.format(this.minutes().remaining),
      };
    },
    format(value) {
      return ("0" + parseInt(value)).slice(-2)
    },
    time(){
      return {
        days: this.days().value.charAt(0),
        hours1: this.hours().value.charAt(0),
        hours2: this.hours().value.charAt(1),
        minutes1: this.minutes().value.charAt(0),
        minutes2: this.minutes().value.charAt(1),
        seconds1: this.seconds().value.charAt(0),
        seconds2: this.seconds().value.charAt(1),
      }
    },
  }
}

function handleInput(e) {
  const radioInput = document.querySelector("input[type=radio]:checked ~ label")
  if (!radioInput) return
  ws.send(
    JSON.stringify({ message: radioInput.textContent })
  );
}


renderBids(cardId);

radioElements.forEach(radioEl => {
  radioEl.addEventListener('click', () => bidSubmitBtn.removeAttribute("disabled"))
})

// messages.scrollTop = messages.scrollHeight;

const ws = new WebSocket(`ws://${window.location.host}/ws/${roomName}/${userRole}/`)
ws.onclose = () => console.error("Chat socket closed unexpectedly");

bidSubmitBtn.onclick = handleInput;

ws.onmessage = e => {

  if (userRole != 'bidder') {
    renderBids(cardId);
    alertText.textContent = "Новая ставка!";
    alertContainer.classList.remove('hidden')
    alertContainer.classList.remove('bidding__alert-hidden')
    return
  }

  const text_data = JSON.parse(e.data).data
  console.log(text_data)

  if (text_data === "await") {
    bidSubmitBtn.setAttribute("disabled", "");
    overlay.removeAttribute("hidden");
    spinner.removeAttribute("hidden");
  } else if (text_data === "ready") {
    renderBids(cardId);
    bidSubmitBtn.removeAttribute("disabled");
  } else if (text_data === "ownbid") {
    alertText.textContent = "Запрещено повышать собственную ставку!";
    alertContainer.classList.remove('hidden')
    alertContainer.classList.remove('bidding__alert-hidden')
  } else if (text_data === "owncompanybid") {
    alertText.textContent = "Запрещено повышать ставку своей компании!";
    alertContainer.classList.remove('hidden')
    alertContainer.classList.remove('bidding__alert-hidden')
  } else {
    alertText.textContent = "Новая ставка!";
    alertContainer.classList.remove('hidden')
    alertContainer.classList.remove('bidding__alert-hidden')
  }
};

// продумать логику выхода при завершении времени