const overlay = document.querySelector('.overlay')
const form = document.querySelector('.modal__form')
const code = document.querySelector('.modal__input')
const codeInputFields = code.querySelectorAll('.modal__code')
const codeAccumulator = document.querySelector('.modal__code__accumulator')
const codeConfirmButton = document.querySelector(".modal__button_confirm")

const KEYBOARDS = {
  backspace: 8,
  arrowLeft: 37,
  arrowRight: 39,
}

function handleBackspace(e) { 
    const input = e.target
    if (input.value) {
        input.value = ''
        return
    }
    input.previousElementSibling.focus()
}

function handleArrowLeft(e) {
    const previousInput = e.target.previousElementSibling
    if (!previousInput) return
    previousInput.focus()
}

function handleArrowRight(e) {
    const nextInput = e.target.nextElementSibling
    if (!nextInput) return
    nextInput.focus()
}

function handleInput(e) {
    const input = e.target
    const nextInput = input.nextElementSibling
    if (nextInput && input.value) {
      nextInput.focus()
      if (nextInput.value) {
        nextInput.select()
      }
    } else if (!nextInput) {
      codeConfirmButton.removeAttribute("disabled")
      codeConfirmButton.focus()
    }
  }
  
codeInputFields.forEach(input => {
    input.addEventListener('focus', e => {
      setTimeout(() => {
        e.target.select()
      }, 0)
    })
  
    input.addEventListener('keydown', e => {
      switch(e.keyCode) {
        case KEYBOARDS.backspace:
          handleBackspace(e)
          break
        case KEYBOARDS.arrowLeft:
          handleArrowLeft(e)
          break
        case KEYBOARDS.arrowRight:
          handleArrowRight(e)
          break
        default:  
      }
    })
})


form.addEventListener('submit', e => {
    e.preventDefault()
    let code = ''
    codeInputFields.forEach(el => code = code.concat(el.value))
    codeAccumulator.value = code
    form.submit()
})

code.addEventListener('input', handleInput)

codeInputFields[0].focus()