function getCookie() {
    let cookieValue = null;
    let name = "csrftoken";
    if (document.cookie && document.cookie !== "") {
        let cookies = document.cookie.split(";");
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === name + "=") {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

class Alert {
    constructor(id, timeout) {
      this.id = id;
      this.timeout = timeout;
    }
    
    show(msg) {
      let timeout = this.timeout;
  
      let msgboxArea = document.querySelector(this.id);
      let msgboxBox = document.createElement("DIV");
      let msgboxContent = document.createElement("DIV");
      let msgboxClose = document.createElement("A");
  
      msgboxContent.classList.add("msgbox-content");
      msgboxContent.innerText = msg;
      
      msgboxClose.classList.add("msgbox-close");
      msgboxClose.setAttribute("href", "#");
      msgboxClose.innerText = 'x';

      msgboxBox.classList.add("msgbox-box");
      msgboxBox.appendChild(msgboxContent);
      msgboxBox.appendChild(msgboxClose);
      msgboxArea.appendChild(msgboxBox);
  
      msgboxClose.addEventListener("click", (e) => {
        e.preventDefault();
        if (msgboxBox.classList.contains("msgbox-box-hide")) {
          return;
        }
        this.hide(msgboxBox);
      });
  
      if (timeout > 0) {
        this.msgboxTimeout = setTimeout(() => {
          this.hide(msgboxBox);
        }, timeout);
      }
    }
    
    hide(msgboxBox) {
      if (msgboxBox !== null) {
        msgboxBox.classList.add("msgbox-box-hide");
      }
      msgboxBox.addEventListener("transitionend", () => {
        if (msgboxBox !== null) {
          msgboxBox.parentNode.removeChild(msgboxBox);
          clearTimeout(this.msgboxTimeout);
        }
      });
    }
  }  


async function getAuctionDetails(auctionId, type) {
    let url_suffix;
    if (type === 'chart-details') {
        url_suffix = 'chart-details'
    } else if (type === 'chart-analysis') {
        url_suffix = 'chart-analysis'
    } else if (type === 'datatable') {
        url_suffix = 'datatable'
    }
    const response = await fetch(`/lots/api/details/${auctionId}/${url_suffix}/`, {
        headers: new Headers({
            "X-CSRFToken": getCookie("csrftoken"),
            "X-Requested-With": "XMLHttpRequest",
        }),
    })
    return response.json()
}

var csrftoken = getCookie('csrftoken');

