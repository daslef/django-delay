const onlineCardsOpened = document.querySelectorAll('.card__online-opened')
const onlineCardsClosed = document.querySelectorAll('.card-closed')

const closedAuctionElements = document.querySelectorAll('.card__online__div-closed:not(.card__online__div-disabled)')

function timer(expiry) {
  return {
    expiry: expiry,
    remaining: null,
    init() {
      this.setRemaining()
      setInterval(() => {
        this.setRemaining();
      }, 1000);
    },
    setRemaining() {
      const diff = this.expiry - new Date().getTime();
      this.remaining =  parseInt(diff / 1000);
    },
    days() {
      return {
        value: this.format(this.remaining / 86400),
        remaining: this.remaining % 86400
      };
    },
    hours() {
      return {
        value: this.format(this.days().remaining / 3600),
        remaining: this.days().remaining % 3600
      };
    },
    minutes() {
      return {
        value: this.format(this.hours().remaining / 60),
        remaining: this.hours().remaining % 60
      };
    },
    format(value) {
      return ("0" + parseInt(value)).slice(-2)
    },
    time(){
      return {
        days1: this.days().value.charAt(0),
        days2: this.days().value.charAt(1),
        hours1: this.hours().value.charAt(0),
        hours2: this.hours().value.charAt(1),
        minutes1: this.minutes().value.charAt(0),
        minutes2: this.minutes().value.charAt(1),
      }
    },
  }
}

function formatPrice(price) {
  if (price === undefined || price == "NaN" || price == "0") {
    return "0"
  }
  return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
}

function renderOnlineData(card, type) {
  if (type === 'opened') {
    let count, last;
    const cardId = card.getAttribute('data-id')
    const countElement = card.querySelector('.card__online__count')
    const lastElement = card.querySelector('.card__online__last')
    const ctaElement = card.querySelector('.card__online__data-cta')
  
    const url = `/lots/api/info/${cardId}`
    fetch(url, {
      headers: new Headers({
        "X-CSRFToken": getCookie("csrftoken"),
        "X-Requested-With": "XMLHttpRequest",
      }),
    })
    .then(response => response.json())
    .then(data => {
      if (data != 'No Bids') {
        count = data.count
        last = data.last
        countElement.innerHTML = `Сделано ставок: <strong>${count}</strong>`
        lastElement.innerHTML = `Текущая: <strong>${formatPrice(last)}</strong>`
        if (ctaElement) ctaElement.innerHTML = ''
      }
    })
  } else if (type === 'closed') {
    const cardId = card.querySelector('.card__online').getAttribute('data-id')
    const countElement = card.querySelector('.card__online__count')
    const url = `/lots/api/info/${cardId}`
    fetch(url, {
      headers: new Headers({
        "X-CSRFToken": getCookie("csrftoken"),
        "X-Requested-With": "XMLHttpRequest",
      }),
    })
    .then(response => response.json())
    .then(data => countElement.innerHTML = `Ставок: <strong>${data.count}</strong>`)
  }
  console.log(`called with ${type}`)
}

onlineCardsOpened.forEach(function(card) {
  setInterval(() => renderOnlineData(card, 'opened'), 1000 * 6)
})

onlineCardsClosed.forEach(function(card) {
  setInterval(() => renderOnlineData(card, 'closed'), 1000 * 20)
})

let favoriteElements = document.querySelectorAll(".favorite");
if (favoriteElements !== undefined) {
  favoriteElements.forEach(favoriteEl => {
    favoriteEl.addEventListener('click', function() {
      let icon = this.querySelector('svg')
      let cardId = this.closest(".card").id
      let favCounter = this.querySelector("text")
      icon.classList.toggle("clicked")

      fetch(`/lots/api/favorite/${cardId}`, {
        headers: new Headers({
          "X-CSRFToken": getCookie("csrftoken"),
          "X-Requested-With": "XMLHttpRequest",
        }),
      })
      .then(response => response.json())
      .then(data => favCounter.innerHTML = data.count)
      .then(() => {
        const alertBox = new Alert("#msgbox-area", 3000);
        alertBox.show('Изменения сохранены')
        delete alertBox
      })
    });
  })
}


closedAuctionElements.forEach(el => {
  const el_input = el.querySelector('input')
  const el_button = el.querySelector('div')
  el_button.classList.add('card__online__button-closed-disabled')

  const cardId = el.parentElement.getAttribute('data-id')
  const url = `/lots/api/info/${cardId}`
  fetch(url, {
    headers: new Headers({
      "X-CSRFToken": getCookie("csrftoken"),
      "X-Requested-With": "XMLHttpRequest",
    }),
  })
  .then(response => response.json())
  .then(data => {
      el_input.value = data.lastUserBid
      el_input.setAttribute('data-initial', el_input.value)
  })

  el_input.addEventListener('input', (e) => {
    let rawValue = parseInt(e.target.value)
    if (rawValue > parseInt(e.target.getAttribute('data-initial'))) {
      el_button.classList.remove('card__online__button-closed-disabled')
      el_button.classList.add('card__online__button-closed-enabled')
      el_button.removeAttribute('aria-label')
      el_button.removeAttribute('data-balloon-pos')
    } else {
      el_button.classList.add('card__online__button-closed-disabled')
      el_button.setAttribute('aria-label', "Сумма ставки некорректна")
      el_button.setAttribute('data-balloon-pos', "up")
      el_button.classList.remove('card__online__button-closed-enabled')    
    }
  })

  el_button.addEventListener('click', e => {
    if (el_button.classList.contains('card__online__button-closed-enabled')) {
      const url = `api/bid/${cardId}/${el_input.value.replace(/ /g, '')}/`
      let message;
      fetch(url, {
        headers: new Headers({
        "X-CSRFToken": getCookie("csrftoken"),
        "X-Requested-With": "XMLHttpRequest",
        })
      })
        .then(response => response.json())
        .then(data => {
          message = data.message
          if (data.code === 'success') {
            location.reload()
          } else {
            const alertBox = new Alert("#msgbox-area", 3000);
            alertBox.show(message)
          }
        })
    }
  })

})