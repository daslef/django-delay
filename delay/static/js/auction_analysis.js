function datatables() {
    return {
        headings: [{
                'key': 'region',
                'value': 'Регион'
            },
            {
                'key': 'age',
                'value': 'Возраст'
            },
            {
                'key': 'sex',
                'value': 'Пол'
            },
            {
                'key': 'sumOfLoan',
                'value': 'Сумма кредита'
            },
            {
                'key': 'osz',
                'value': 'ОСЗ'
            },
            {
                'key': 'od',
                'value': 'ОД'
            },
            {
                'key': 'delayPeriod',
                'value': 'Срок просрочки (дней)'
            },
            {
                'key': 'lastPayment',
                'value': 'Последний платеж'
            },
            {
                'key': 'judgeAct',
                'value': 'Судебный акт'
            },
            {
                'key': 'id',
                'value': 'ИД'
            },
        ],
        cols: [],
        open: false,
        init() {
            fetch(`/lots/api/${cardId}/datatable/`, {
                    headers: new Headers({
                        'X-CSRFToken': getCookie('csrftoken'),
                        'X-Requested-With': 'XMLHttpRequest',
                    }),
                })
                .then(response => response.json())
                .then(data => this.cols = data)
        },
        toggleColumn(key) {
            let columns = document.querySelectorAll('.' + key);
            if (this.$refs[key].classList.contains('hidden') && this.$refs[key].classList.contains(key)) {
                columns.forEach(column => column.classList.remove('hidden'));
            } else {
                columns.forEach(column => column.classList.add('hidden'));
            }
        }
    }
}

async function plotSexDonut(data) {
    const count_men = data.filter(sex => sex === 'м').length
    const count_women = data.filter(sex => sex === 'ж').length
    const options = {
        series: [count_men, count_women],
        colors: ['#33B2DF', '#543E7A', '#D4526E', '#13D8AA', '#A5978B'],
        chart: {
            type: 'donut',
            fontFamily: "Fraunces, Cormorant, sans-serif",
        },
        legend: {
            position: 'bottom'
        },
        labels: ['Мужчины', 'Женщины'],
        title: {
            text: 'Распределение по полу',
            align: 'center'
        }
    };

    const sexDonutChart = new ApexCharts(document.querySelector(".sex-plot"), options);
    sexDonutChart.render();
}

async function plotAgeDonut(data) {
    const ages = [0,0,0,0,0]
    for (const age of data) {
        if (Number(age) < 25) {
            ages[0] += 1
        } else if (Number(age) < 40) {
            ages[1] += 1
        } else if (Number(age) < 60) {
            ages[2] += 1
        } else if (Number(age) < 75) {
            ages[3] += 1
        } else {
            ages[4] += 1
        }
    }
    const options = {
        series: ages,
        colors: ['#33B2DF', '#543E7A', '#D4526E', '#13D8AA', '#A5978B'],
        chart: {
            type: 'donut',
            fontFamily: "Fraunces, Cormorant, sans-serif",
        },
        plotOptions: {
            pie: {
                donut: {
                    labels: {
                        value: {
                            color: "#2E294E",
                            fontSize: '24px',
                        },
                    },
                },
            },
        },
        legend: {
            position: 'bottom'
        },
        labels: ['<25', '25-40', '40-60', '60-75', '>75'],
        title: {
            text: 'Распределение по возрастам',
            align: 'center'
        },
    };
    const ageDonutChart = new ApexCharts(document.querySelector(".age-plot"), options);
    ageDonutChart.render();
}


async function plotDelayBar(data) {
    const delayLengths = [0,0,0,0,0]
    for (const delay of data) {
        if (Number(delay) < 183) {
            delayLengths[0] += 1
        } else if (Number(delay) < 366) {
            delayLengths[1] += 1
        } else if (Number(delay) < 732) {
            delayLengths[2] += 1
        } else if (Number(delay) < 1080) {
            delayLengths[3] += 1
        } else {
            delayLengths[4] += 1
        }
    }

    const options = {
        series: [{
            name: 'Количество',
            data: delayLengths
        }],
        colors: ['#33B2DF', '#543E7A', '#D4526E', '#13D8AA', '#A5978B'],
        chart: {
            type: 'bar',
            toolbar: {
                show: false,
            },
            fontFamily: "Fraunces, Cormorant, sans-serif",
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top',
                },
                barHeight: '60%',
            }
        },
        dataLabels: {
            enabled: true,
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        xaxis: {
            categories: ["< 6 мес.", "6-12 мес.", "1-2 года", "2-3 года", "> 3 лет"],
            position: 'top',
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
            }
        },
        title: {
            text: 'Срок просрочки',
            align: 'center'
        }
    }
    const delayBarChart = new ApexCharts(document.querySelector(".delay-plot"), options);
    delayBarChart.render();
}


async function plotOszBar(data) {
    const oszAmounts = [0,0,0,0,0,0]
    for (const oszAmount of data) {
        if (Number(oszAmount) < 10000) {
            oszAmounts[0] += 1
        } else if (Number(oszAmount) < 50000) {
            oszAmounts[1] += 1
        } else if (Number(oszAmount) < 100000) {
            oszAmounts[2] += 1
        } else if (Number(oszAmount) < 200000) {
            oszAmounts[3] += 1
        } else if (Number(oszAmount) < 300000) {
            oszAmounts[4] += 1
        } else {
            oszAmounts[5] += 1
        }
    }

    const options = {
        series: [{
            name: 'Количество',
            data: oszAmounts
        }],
        colors: ['#33B2DF', '#543E7A', '#D4526E', '#13D8AA', '#A5978B'],
        chart: {
            type: 'bar',
            toolbar: {
                show: false,
            },
            fontFamily: "Fraunces, Cormorant, sans-serif",
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    position: 'top',
                },
            }
        },
        dataLabels: {
            enabled: true,
            offsetY: -20,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        xaxis: {
            categories: ["< 10 тыс.", "10-50 тыс.", "50-100 тыс.", "100-200 тыс.", "200-300 тыс.", "> 300 тыс."],
            position: 'top',
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            crosshairs: {
                fill: {
                    type: 'gradient',
                    gradient: {
                        colorFrom: '#D8E3F0',
                        colorTo: '#BED1E6',
                        stops: [0, 100],
                        opacityFrom: 0.4,
                        opacityTo: 0.5,
                    }
                }
            },
        },
        yaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false,
            },
            labels: {
                show: false,
            }
        },
        title: {
            text: 'Основная сумма задолженности',
            align: 'center'
        }
    }
    const oszBarChart = new ApexCharts(document.querySelector(".osz-plot"), options);
    oszBarChart.render();
}



async function plotRegionDonut(data) {
    const uniqueRegions = [...new Set(data)]
    const series = []
    const labels = []
    uniqueRegions.map(region => {
        labels.push(region)
        series.push(data.filter(entry => entry === region).length)
    })
    const options = {
        series: series,
        colors: ['#33B2DF', '#543E7A', '#D4526E', '#13D8AA', '#A5978B'],
        chart: {
            type: 'donut',
            fontFamily: "Fraunces, Cormorant, sans-serif",
        },
        legend: {
            position: 'bottom'
        },
        labels: labels,
        title: {
            text: 'Распределение по регионам',
            align: 'center'
        }
    };

    const regionDonutChart = new ApexCharts(document.querySelector(".region-plot"), options);
    regionDonutChart.render();
}

async function plotCharts(cardId) {
    spinner.removeAttribute('hidden');
    let response = await getAuctionDetails(cardId, 'chart-analysis')
    let { sex, age, region, osz, delay } = response
    await plotSexDonut(sex)
    await plotRegionDonut(region)
    await plotAgeDonut(age)
    await plotOszBar(osz)
    await plotDelayBar(delay)
    spinner.setAttribute('hidden', '');

}

const cardId = JSON.parse(document.getElementById("card-id").textContent);
const spinner = document.querySelector('.spinner')

plotCharts(cardId)
