const phone = document.querySelector(".login__phone")
const password = document.querySelector(".login__password")
let phoneInput = document.querySelector(".login__phone__input")
const passwordInput = document.querySelector(".login__password__input")

const mask = IMask(phoneInput, {
  mask: '+{7}(000)000-00-00',
  lazy: false,
  placeholderChar: '∙'
});

window.form = () => {
  return {
    inputElements: [],
    init: function () {
      Iodine.addRule('phone', (value) => {
        const regex = /^\+7\(\d\d\d\)\d\d\d\-\d\d\-\d\d$/;
        return regex.test(value)
      })
      Iodine.setErrorMessages({ 
        phone: 'Введите корректный номер',
        minimum: `Минимальная длина - '[PARAM]' символов`,
        required: `Заполните поле`,
      });
      this.inputElements = [...this.$el.querySelectorAll("input[data-rules]")];
      this.initDomData();
      this.updateErrorMessages();
    },
    initDomData: function () {
      this.inputElements.map((ele) => {
        this[ele.name] = {
          serverErrors: JSON.parse(ele.dataset.serverErrors),
          blurred: false
        };
      });
    },
    updateErrorMessages: function () {
      this.inputElements.map((ele) => {
        this[ele.name].errorMessage = this.getErrorMessage(ele);
      });
    },
    getErrorMessage: function (ele) {
      if (this[ele.name].serverErrors.length > 0) {
        return input.serverErrors[0];
      }
      const error = Iodine.is(ele.value, JSON.parse(ele.dataset.rules));
      if (error !== true && this[ele.name].blurred) {
        return Iodine.getErrorMessage(error);
      }
      return "";
    },
    submit: function (event) {
      const invalidElements = this.inputElements.filter((input) => {
        return Iodine.is(input.value, JSON.parse(input.dataset.rules)) !== true;
      });
      if (invalidElements.length > 0) {
        event.preventDefault();
        document.getElementById(invalidElements[0].id).scrollIntoView();
        this.inputElements.map((input) => {
          this[input.name].blurred = true;
        });
        this.updateErrorMessages();
      } else {
        phoneInput.value = '+'.concat(mask.unmaskedValue)
      }
    },
    change: function (event) {
      if (!this[event.target.name]) {
        return false;
      }
      if (event.type === "input") {
        this[event.target.name].serverErrors = [];
      }
      if (event.type === "focusout") {
        this[event.target.name].blurred = true;
      }
      this.updateErrorMessages();
    }
  };
};
