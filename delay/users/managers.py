from django.contrib.auth.base_user import BaseUserManager

class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, phone, password, last_name, first_name, company_name, company_type, **extra_fields):
        values = [email, phone, last_name, first_name, company_name]
        field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
        for field_name, value in field_value_map.items():
            if not value:
                raise ValueError('The {} value must be set'.format(field_name))

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            phone=phone,
            last_name=last_name,
            first_name=first_name,
            company_name=company_name,
            company_type=company_type,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, phone, last_name, first_name, company_name, company_type, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, phone, password, last_name, first_name, company_name, company_type, **extra_fields)

    def create_superuser(self, email, phone, last_name, first_name, company_name, company_type, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        return self._create_user(email, phone, password, last_name, first_name, company_name, company_type, **extra_fields)
