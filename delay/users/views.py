import json

from django.views.generic import CreateView
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import LoginView
from django.contrib.auth.hashers import check_password
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.core.cache import cache
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy

from delay.helpers import ajax_required, generate_code, send_sms

from .models import User
from .forms import CustomUserCreationForm


class SignUpView(CreateView):
    template_name = 'users/account/signup.html'
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('users:login')


def user_login_view(request):
    if request.method == 'GET':
        return render(request, "users/account/login.html", context={'error': None})
    phone = request.POST.get('username')
    password = request.POST.get('password')
    try:
        user = User.objects.get(phone=phone)
    except Exception:
        return render(request, "users/account/login.html", context={'error': 'Аккаунт не найден'})
    if not check_password(password, user.password):
        return render(request, "users/account/login.html", context={'error': 'Неверный пароль'})
    request.session['phone'] = phone
    request.session['password'] = password
    return redirect('users:verify')


def user_verify_view(request):
    if request.method == 'GET':
        code = '22222' # generate_code()
        phone = request.session['phone']
        cache.set(f'{phone}_otp', code, 120)
        # send_sms(code, '+79969752375') # phone
        print(phone, cache.get(f'{phone}_otp'))
        return render(request, "users/account/verify.html")
    code = request.POST.get('code')
    print(code)
    phone = request.session['phone']
    correct_code = cache.get(f'{phone}_otp')
    if not correct_code:
        return render(request, "users/account/verify.html", context={'error': "Срок действия кода вышел. Мы отправили Вам новый"})
    if correct_code != code:
        return render(request, "users/account/verify.html", context={'error': "Неверный код. Мы отправили Вам новый"})
    user = authenticate(request, username=phone, password=request.session['password'])
    if not user:
        return render(request, "users/account/login.html", context={'error': 'Возникли проблемы при входе в аккаунт. Попробуйте еще раз'})
    login(request, user)
    print(user)
    return redirect('lots:all')
