from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models

from .managers import AccountManager

class User(AbstractBaseUser, PermissionsMixin):

    # архив документов - набор отличается в зависимости от роли, в подсказке уточнить

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email', 'last_name', 'first_name', 'company_name', 'company_type']

    COMPANY_TYPES = (
        ('B', 'Банк'),
        ('M', 'Микрофинансовая организация'),
        ('C', "Коллекторское агенство"),
    )

    email = models.EmailField("E-mail")
    phone = models.CharField("Телефон", max_length=20, unique=True)
    last_name = models.CharField("Фамилия", max_length=40)
    first_name = models.CharField("Имя", max_length=40)
    second_name = models.CharField("Отчество", max_length=30,  blank=True, default="")
    company_name = models.CharField("Название организации", max_length=40)
    company_type = models.CharField("Тип организации", choices=COMPANY_TYPES, max_length=1)
    company_logo = models.ImageField("Логотип", upload_to="users_logo/", null=True, blank=True)
    company_about = models.CharField("Краткое описание", max_length=1200, default="", blank=True)
    documents = models.FileField(upload_to="documents/")
    is_seller = models.BooleanField("Продавец", default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField("Активный аккаунт", default=False)
    last_login = models.DateTimeField(null=True, blank=True)

    objects = AccountManager()

    def __str__(self):
        return f'{self.first_name} / {self.phone}'