from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import password_validation

from delay.users.models import User


class CustomUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update(
        {"duplicate_phone": "Данный телефонный номер уже используется в системе"}
    )

    password2 = None

    class Meta:
        model = User
        fields = ['email', 'phone', 'password1', 'last_name', 'first_name', 'company_name', 'company_type', 'documents']

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        try:
            password_validation.validate_password(password1, self.instance)
        except forms.ValidationError as error:
            self.add_error('password1', error)
        return password1

    def clean_phone(self):
        phone = self.cleaned_data["phone"]
        try:
            User.objects.get(phone=phone)
        except User.DoesNotExist:
            return phone.replace('(', '').replace(')', '').replace('-', '')
        raise forms.ValidationError(self.error_messages["duplicate_phone"])
