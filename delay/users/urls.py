from django.urls import path, re_path, reverse_lazy
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import PasswordResetForm

from .views import SignUpView, user_login_view, user_verify_view 

app_name = "users"

urlpatterns = [
    re_path(r"^signup/$", view=SignUpView.as_view(), name="signup"),
    re_path(r'^login/$', view=user_login_view, name='login'),
    re_path(r'^verify/$', view=user_verify_view, name='verify'),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),

    re_path(
        r'^password_reset/$', 
        auth_views.PasswordResetView.as_view(
            form_class=PasswordResetForm,
            template_name='users/account/password_reset_form.html',
            email_template_name='users/account/password_reset_email.html',
            success_url=reverse_lazy('users:password_reset_done')
        ),
        name='password_reset'
    ),

    re_path(
        r'^password_reset/done/$', 
        auth_views.PasswordResetDoneView.as_view(template_name="users/account/password_reset_done.html"), 
        name='password_reset_done'
    ),

    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(
            success_url = reverse_lazy('users:password_reset_complete')
        ),
        name='password_reset_confirm',
    ),

    re_path(
        r'^reset/done/$', 
        auth_views.PasswordResetCompleteView.as_view(), 
        name='password_reset_complete'
    )

]
