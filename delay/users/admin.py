from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from delay.users.models import User
from delay.users.forms import CustomUserCreationForm


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    model = User
    ordering = ('email', )
    list_display = ['phone', 'company_name', 'company_type', 'last_name', 'first_name', 'email', 'is_seller', 'is_active']
    # search_fields = ["name"]
    
    fieldsets = (
        (None, {'fields': ('phone', 'email', 'password', 'is_seller', 'is_active')}),
        ('Personal info', {'fields': ('last_name', 'first_name', 'second_name', 'documents')}),
        ('Company info', {'fields': ('company_name', 'company_type', 'company_about', 'company_logo')}),
    )

    add_fieldsets = (
        (None, {'fields': ('phone', 'email', 'password1', 'password2', 'is_seller', 'is_active')}),
        ('Personal info', {'fields': ('last_name', 'first_name', 'second_name', 'documents')}),
        ('Company info', {'fields': ('company_name', 'company_type', 'company_about', 'company_logo')}),
    )


admin.site.register(User, CustomUserAdmin)
