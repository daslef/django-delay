from rest_framework import serializers

from delay.core.models import Auction

from django.contrib.auth import get_user_model


class AuctionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Auction
        exclude = ('timestamp_start', 'timestamp_finish', 'status', 'has_winner')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)