import React from "react";

import Form from "./components/Form";

import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <main className="container">
        <Form />
      </main>
    </div>
  );
}