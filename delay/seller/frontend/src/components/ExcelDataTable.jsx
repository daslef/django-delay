import React, {useState, useEffect} from "react";

export default function ExcelDataTable({excelData}) {

  console.log(excelData)

  let tableData = [
    { name: "Количество дел", value: excelData.n_cases },
    { name: "Регион", value: excelData.region },
    { name: "Мультирегион", value: excelData.multiregion },
    { name: "ОСЗ", value: excelData.osz },
    { name: "ОД", value: excelData.od },
    { name: "Средний долг", value: excelData.average_debt },
    { name: "Средний срок", value: excelData.average_delay }
  ];

  let tableItems = tableData.map((item) => (
    <tr>
      <th scope="row">{item.name}</th>
      <td data-label={item.name}>{item.value}</td>
    </tr>
  ));

  return (
    <div className="card-auction-content">
      <table>
        <tbody>{tableItems}</tbody>
      </table>
    </div>
  );
}
