import React from "react";

export default function FormDocumentFlow({onChange}) {
  return (
    <div id="div_id_document_flow" className="form-group">
      <label htmlFor="id_document_flow" className=" requiredField">
        Форма передачи документов
        <span className="asteriskField">*</span>
      </label>
      <div className="">
        <select
          name="document_flow"
          className="select form-control"
          id="id_document_flow"
          onChange={onChange}
        >
          <option value="E" defaultValue>
            Электронная
          </option>
          <option value="P">Печатная</option>
        </select>
      </div>
    </div>
  );
}
