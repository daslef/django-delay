import React from 'react'

export default function ForbSubmitButton(props){
    return (
        <input
            type="submit"
            name="submit"
            value="Отправить заявку"
            className="btn btn-primary"
            id="submit-id-sumbit"
      />
    )
}