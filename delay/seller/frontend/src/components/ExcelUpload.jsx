import React, { useState } from "react";
import { getCookie, CSRFToken } from "./csrf";

async function postData(fileObject) {
  let formData = new FormData();
  formData.set("file", fileObject);

  let response = await fetch("http://localhost:8000/seller/api/create-auction/", {
    method: "POST",
    headers: new Headers({
      "X-CSRFToken": getCookie("csrftoken"),
      "X-Requested-With": "XMLHttpRequest"
    }),
    body: formData
  })

  let outputData = response.json()
  return outputData;
};

export default function ExcelUpload(props) {
  let [fileWasUploaded, setFileWasUploaded] = useState(false);
  let [fileName, setFileName] = useState("");

  async function changeHandle(e) {
    let file = e.target.files[0]
    setFileName(file.name);
    setFileWasUploaded(true);
    let fetchedData = await postData(file);
    props.loadFile(file)
    props.loadData(fetchedData);
  };

  return (
    <div
      className="file-upload-wrapper mb-3"
      data-text={fileWasUploaded ? fileName : "Выберите файл"}
    >
      <input
        name="document"
        type="file"
        className="file-upload-field"
        onChange={changeHandle}
        value=""
      />
    </div>
  );
}
