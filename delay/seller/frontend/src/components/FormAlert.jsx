import React from "react";

export default function FormAlert(props) {
  return <div className="alert alert-info">{props.message}</div>
}
