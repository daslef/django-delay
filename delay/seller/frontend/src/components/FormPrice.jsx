import React from "react";

export default function FormPrice(props) {
  return (
    <div id="div_id_start_price" className="form-group">
      <label htmlFor="id_start_price" className=" requiredField">
        Стартовая ставка
        <span className="asteriskField">*</span>
      </label>
      <div className="">
        <div className="input-group">
          <input
            type="number"
            name="start_price"
            step="1"
            className="numberinput form-control"
            required
            id="id_start_price"
            onChange={props.onChange}
          />
          <div className="input-group-append active">
            <span className="input-group-text">₽</span>
          </div>
        </div>
      </div>
    </div>
  );
}
