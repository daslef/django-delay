import React, { useState } from "react";

import { CSRFToken, getCookie } from "./csrf";
import ExcelDataTable from "./ExcelDataTable";
import FormAlert from "./FormAlert";
import FormDocuments from "./FormDocuments";
import ExcelUpload from "./ExcelUpload";
import FormCheckboxFields from "./FormCheckboxFields";
import FormSubmitButton from './FormSubmitButton'
import FormPrice from "./FormPrice";
import FormDocumentFlow from "./FormDocumentFlow"
import FormAllowed from "./FormAllowed"
import FormCheckboxIsOpen from "./FormCheckboxIsOpen"

export default function Form(props) {
  const [excelDataLoaded, setExcelDataLoaded] = useState(false)
  const [excelData, setExcelData] = useState({});

  const [formDataValues, setFormDataValues] = useState({})

  function handleSubmit(e) {
    e.preventDefault()
    let formData = new FormData()
    for (let item of Object.entries(formDataValues)) {
      formData.append(item[0], item[1])
    }
    let multiregion = formData.get('multiregion') === 'Да' ? 'True' : 'False'
    formData.set('multiregion', multiregion)
    console.log(formData)

    fetch("http://localhost:8000/seller/api/create-auction-2/", {
      method: "POST",
      headers: new Headers({
        "X-CSRFToken": getCookie("csrftoken"),
        "X-Requested-With": "XMLHttpRequest",
      }),
      body: formData
    })
    .then(response => response.json())
    .then(data => console.log(data))
  }

  return (
    <form
      id="new-auction-form"
      encType="multipart/form-data"
      onSubmit={handleSubmit}
    >
      <ExcelUpload 
        loadData={(data) => {
          data = JSON.parse(data)
          console.log(data)
          setExcelData({
            osz: data.osz,
            region: data.region,
            od: data.od,
            average_debt: data.average_debt,
            average_delay: data.average_delay,
            multiregion: data.multiregion,
            n_cases: data.n_cases
          })
          setExcelDataLoaded(true)
          setFormDataValues({...formDataValues, ...data})
        }}
        loadFile={(file) => setFormDataValues({...formDataValues, file: file})} 
      />
      <CSRFToken />
      <FormAlert message={excelDataLoaded ? "Проверьте правильность распознавания документа. В случае несоответствия вернитесь на прошлый шаг" : "Загрузите реестр в формате xls/xlsx"}/>
      <ExcelDataTable excelData={excelDataLoaded ? excelData : "..."} />
      <div className="form-row ">
        <div className="col-md-5 d-flex flex-column justify-content-between">
          <FormPrice 
            onChange={e => setFormDataValues({ ...formDataValues, start_price: e.target.valueAsNumber })} 
          />
          <FormDocumentFlow 
            onChange={e => setFormDataValues({ ...formDataValues, document_flow: e.target.value }) }
          />
          <FormAllowed />
          <FormCheckboxFields />
          <FormSubmitButton />
        </div>
        <div className="col-md-7 d-flex flex-column justify-content-between">
          <FormDocuments 
            handleChange={e => {
              let fileInput = e.target
              let fileCategory = fileInput.name
              let fileSrc =  fileInput.files[0]
              if (fileCategory === "doc_example_assignment_agreement") {
                setFormDataValues({...formDataValues, doc_example_assignment_agreement: fileSrc })
              } else if (fileCategory === "doc_impersonal_loan_agreement") {
                setFormDataValues({...formDataValues, doc_impersonal_loan_agreement: fileSrc })
              } else if (fileCategory === "doc_personal_account_statement") {
                setFormDataValues({...formDataValues, doc_personal_account_statement: fileSrc })
              }
            }}
          />
          <div id="div_id_description" className="form-group">
            <label htmlFor="id_description" className=" requiredField">
              Комментарии<span className="asteriskField">*</span>
            </label>
            <div className="">
              <textarea
                name="description"
                cols="40"
                rows="10"
                placeholder="Приведите свои комментарии по дате и времени проведения аукциона, допуску участников, форме проведения"
                className="textarea form-control"
                required
                onChange={(e) => setFormDataValues({...formDataValues, description: e.target.value})}
                id="id_description"
              ></textarea>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
