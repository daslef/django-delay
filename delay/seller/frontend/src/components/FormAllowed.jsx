import React from 'react';

import { Box, CheckBox, Grommet, Select } from 'grommet';


const Option = React.memo(({ value, selected }) => (
  <Box direction="row" gap="small" align="center" pad="xsmall">
    <CheckBox tabIndex="-1" checked={selected} onChange={() => {}} />
    {value}
  </Box>
));

export default function FormAllowed() {
  const [selected, setSelected] = React.useState([]);
  const [options, setOptions] = React.useState([
      'Банки',
      'Микрофинансовые Организации',
      'Коллекторские агенства',
  ]);

  return (
    <div id="div_id_start_price" className="form-group">
    <label htmlFor="id_start_price" className=" requiredField">
      Допущенные организации
      <span className="asteriskField">*</span>
    </label>
    <div className="">
      <div className="input-group">
      <Select
          multiple
          closeOnChange={false}
          placeholder=""
          selected={selected}
          options={options}
          dropHeight="small"
          onClose={() =>
            setOptions(
              options.sort((p1, p2) => {
                const p1Exists = selected.includes(p1);
                const p2Exists = selected.includes(p2);

                if (!p1Exists && p2Exists) {
                  return 1;
                }
                if (p1Exists && !p2Exists) {
                  return -1;
                }
                return p1.localeCompare(p2, undefined, {
                  numeric: true,
                  sensitivity: 'base',
                });
              }),
            )
          }
          onChange={({ selected: nextSelected }) => {
            setSelected(nextSelected);
          }}
        >
          {(option, index) => (
            <Option value={option} selected={selected.indexOf(index) !== -1} />
          )}
        </Select>
      </div>
    </div>
  </div>

  );
};
