import React from "react";

let items = [
  {
    h2: "Обезличенный договор цессии (пример)",
    fileName: "doc_example_assignment_agreement",
    id: "cb-1"
  },
  {
    h2: "Обезличенный договор займа (пример)",
    fileName: "doc_impersonal_loan_agreement",
    id: "cb-2",
  },
  {
    h2: "Выписка по лицевому счету (пример)",
    fileName: "doc_personal_account_statement",
    id: "cb-3",
  }
];

function FormDocumentsItem({ fileName, id, h2, handleChange }) {

  return (
    <div className="checkcard">
      <input type="file" id={id} name={fileName} onChange={handleChange} hidden />
      <label htmlFor={id}>{h2}</label>
    </div>
  );
}

export default function FormDocuments({handleChange}) {
  return (
    <fieldset className="documents-container">
      <legend>Документы</legend>
      {items.map((item, key) => (
        <FormDocumentsItem key={key} {...item} handleChange={handleChange} />
      ))}
    </fieldset>
  );
}
