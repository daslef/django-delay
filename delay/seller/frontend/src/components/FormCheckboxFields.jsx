import React from "react";

export default function FormCheckboxFields(props) {
  return (
    <>
      <div className="form-group">
        <div id="div_id_postsale" className="custom-control custom-checkbox">
          <input
            type="checkbox"
            name="postsale"
            className="checkboxinput custom-control-input"
            id="id_postsale"
            defaultChecked
          />
          <label htmlFor="id_postsale" className="custom-control-label">
            Коммуникация с покупателем в Delay
          </label>
        </div>
      </div>
      <div className="form-group">
        <div id="div_id_in_pledge" className="custom-control custom-checkbox">
          <input
            type="checkbox"
            name="in_pledge"
            className="checkboxinput custom-control-input"
            id="id_in_pledge"
          />
          <label htmlFor="id_in_pledge" className="custom-control-label">
            Реестр в залоге
          </label>
        </div>
      </div>
      <div className="form-group">
        <div
          id="div_id_rights_assignment"
          className="custom-control custom-checkbox"
        >
          <input
            type="checkbox"
            name="rights_assignment"
            className="checkboxinput custom-control-input"
            id="id_rights_assignment"
          />
          <label htmlFor="id_rights_assignment" className="custom-control-label">
            Вы являетесь первоначальным кредитором
          </label>
        </div>
      </div>
    </>
  );
}
