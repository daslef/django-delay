from django.apps import AppConfig


class SellerConfig(AppConfig):
    name = "delay.seller"
    verbose_name = "Seller"
