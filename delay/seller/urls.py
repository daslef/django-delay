from django.conf.urls import url

from delay.seller.views import (
    create_auction,
    api_create_auction,
    api_create_auction_2 
)

app_name = "seller"
urlpatterns = [
    url(r"^create-auction/$", create_auction, name="create"),
    url(r"^api/create-auction-2/$", api_create_auction_2, name="create_api_2"),
    url(r"^api/create-auction/$", api_create_auction, name="create_api"),
]
