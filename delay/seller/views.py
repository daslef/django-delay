import json

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from django.views.decorators.csrf import ensure_csrf_cookie
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.core.files import File


from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser


from delay.helpers import AuthorRequiredMixin, _process_excel
from delay.core.models import Auction, AuctionQuerySet

from .serializers import AuctionSerializer

from config.settings.base import MEDIA_URL


@ensure_csrf_cookie
def create_auction(request, *args, **kwargs):
    form = AuctionDocumentForm()
    return render(request, "auction/auction_create.html", {"form": form})


@api_view(['POST'])
@parser_classes([MultiPartParser, FormParser, FileUploadParser])
def api_create_auction(request, *args, **kwargs):
    file_obj = request.data['file']
    
    osz, od, region, average_debt, average_delay, n_cases, multiregion = _process_excel(file_obj)
    data = {'osz': osz, 'od': od, 'region': region, 'average_debt': average_debt, 'average_delay': average_delay, 'n_cases': n_cases, 'multiregion': multiregion}
    return Response(json.dumps(data))


@api_view(['POST'])
@parser_classes([MultiPartParser, FormParser])
def api_create_auction_2(request, *args, **kwargs):
    serializer = AuctionSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save()

    return Response('OK', 200)