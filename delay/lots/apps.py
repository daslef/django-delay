from django.apps import AppConfig


class LotsConfig(AppConfig):
    name = "delay.lots"
    verbose_name = "Lots"
