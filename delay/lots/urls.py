from django.conf.urls import url
from django.urls import path

from delay.lots import views

app_name = "lots"

urlpatterns = [
    path("", views.AuctionRelevantListView.as_view(), name="relevant"),
    path("current/", views.AuctionCurrentListView.as_view(), name="current"),
    path("all/", views.AuctionAllListView.as_view(), name="all"),
    path("details/<int:id>/", views.detail_view, name="details"),
    path("details/<int:id>/charts/", views.charts_view, name="charts"),
    path("bidding/<int:id>/", views.bidding_view, name="bidding"),

    path("api/bids/<id>/", views.bids, name="bids"),
    path("api/bid/<id>/<int:value>/", views.bid, name="bid"),
    path("api/favorite/<id>/", views.favorite, name="favorite"),
    path("api/info/<id>/", views.get_online_data, name="online"),
    path("api/details/<id>/<type>/", views.get_auction_data, name="details"),
]
