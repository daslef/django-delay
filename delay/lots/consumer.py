from channels.db import database_sync_to_async
from django.db import transaction
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from delay.core.models import Bid, Auction

class BiddingConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        self.user = self.scope["user"]
        self.auction_id, self.user_role = self.scope["path"].strip('/ws').split('/')
        
        self.individual_group = f'{self.auction_id}-{self.user.id}'
        self.broadcast_group = self.auction_id

        if self.user_role == 'bidder':
            self.bidders_group = f'{self.auction_id}-{self.user_role}'
            await self.channel_layer.group_add(self.bidders_group, self.channel_name)
        
        await self.channel_layer.group_add(self.individual_group, self.channel_name)
        await self.channel_layer.group_add(self.broadcast_group, self.channel_name)
        await self.accept()


    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.individual_group_name, self.channel_name
        )
        await self.channel_layer.group_discard(
            self.broadcast_group, self.channel_name
        )
        if self.user_role == 'bidder':
            await self.channel_layer.group_discard(
               self.bidders_group, self.channel_name
            )


    async def receive_json(self, content):
        message = content.get('message').replace(' ', '')

        await self.channel_layer.group_send(
            group=f'{self.auction_id}-{self.user_role}', message={
                "type": "echo_message",
                "data": "await"
            }
        )
        await self._check_bid(message)
        await self.channel_layer.group_send(
            group=self.bidders_group, message={
                "type": "echo_message",
                "data": "ready"
            }
        )

    async def echo_message(self, message):
        await self.send_json(message)


    @database_sync_to_async
    def _get_last_bid(self):
        auction = Auction.objects.get(id=self.auction_id)
        auction_bids = auction.bids.all()
        last_bid = auction_bids.first()
        if not last_bid:
            last_bid = auction.start_price
        print(last_bid) # ???
        return last_bid

    
    @database_sync_to_async
    def _save_new_bid(self, last_bid, message):
        auction = Auction.objects.get(id=self.auction_id)
        new_bid_step = message
        new_bid_price = last_bid.price + int(message)
        new_bid_db = Bid(user=self.user, price=new_bid_price, step=new_bid_step)
        print(new_bid_db)
        try:
            new_bid_db.save()
            auction.bids.add(new_bid_db)
            auction.save()
        except Exception:
            transaction.rollback()
        return new_bid_price


    async def _check_bid(self, message):
        last_bid = await self._get_last_bid()
        if last_bid.user == self.user:
            await self.channel_layer.group_send(
                self.individual_group, {
                    "type": "echo_message",
                    'data': "ownbid"
                }
            )
            return
            
        elif last_bid.user.company_name == self.user.company_name:
            await self.channel_layer.group_send(
                self.individual_group, {
                    "type": "echo_message",
                    'data': "owncompanybid"
                }
            )
            return

        else:
            try:
                new_bid_price = await self._save_new_bid(last_bid, message)
                await self.channel_layer.group_send(
                    self.broadcast_group, {
                        "type": "echo_message",
                        'data': new_bid_price
                    }
                )
            except Exception as e:
                print(e)