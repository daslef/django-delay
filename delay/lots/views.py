from django.db.utils import IntegrityError
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic import ListView
from django.conf import settings
from django.http import JsonResponse, HttpResponseNotFound, Http404
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render

from delay.helpers import ajax_required, serialize_excel
from delay.core.models import Auction, Bid
from delay.helpers import is_owner


class BaseAuctionListView(ListView):
    model = Auction
    context_object_name = "data"
    template_name = "auction/lots.html"

    def get_queryset(self, user, auctions, **kwargs):

        if user.is_staff:
            return auctions.order_by('status', '-timestamp_finish')

        if user.is_anonymous:
            return auctions.order_by('status', '-timestamp_finish')

        user_company_type = user.company_type
        user_own_auctions = auctions.filter(user=user)
            
        if user_company_type == 'B':
            qs = auctions.filter(accepted_banks=True)
        elif user_company_type == 'M':
            qs = auctions.filter(accepted_mfo=True)
        elif user_company_type == 'C':
            qs = auctions.filter(accepted_collection_agencies=True)

        return qs.union(user_own_auctions).order_by('status', '-timestamp_finish') # TODO продумать сортировку?


class AuctionAllListView(BaseAuctionListView):
    def get_queryset(self, **kwargs):
        auctions = Auction.objects.get_all()
        return super().get_queryset(self.request.user, auctions)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "all"
        return context


class AuctionCurrentListView(BaseAuctionListView):
    def get_queryset(self, **kwargs):
        auctions = Auction.objects.get_started()
        return super().get_queryset(self.request.user, auctions)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "current"
        return context


class AuctionRelevantListView(BaseAuctionListView):
    def get_queryset(self, **kwargs):
        qs_1 = Auction.objects.get_approved()
        qs_2 = Auction.objects.get_started()
        auctions = qs_1.union(qs_2)
        return super().get_queryset(self.request.user, auctions)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["active"] = "relevant"
        return context


@login_required
def detail_view(request, id):
    db_data = Auction.objects.get(id=id)
    if db_data.status not in ['2_S', '1_A']:
        return render(request, "error_pages/404.html")
    return render(
        request, "auction/auction_details.html", context={'card': db_data}
    )


@login_required
def bidding_view(request, id):
    auction = Auction.objects.get(id=id)
    if auction.status != '2_S':
        return render(request, "error_pages/404.html")
    is_allowed = request.user.company_type in auction.allowed_company_types
    is_auction_owner = request.user == auction.user
    if is_auction_owner:
        role = 'author'
    elif is_allowed: # в продакшене фильтровать стаффов
        role = 'bidder'
    else:
        role = 'spectator'

    return render(
        request, "auction/auction_bidding.html", context={'card': auction, 'room_name': id, 'role': role}
    )


@ajax_required
@require_http_methods(["GET"])
def bids(request, id):
    auction = Auction.objects.get(id=id)
    bids = auction.bids.all()
    response = {}
    for ix, bid in enumerate(bids):
        response[ix] = [bid.user.phone, bid.price, bid.step, bid.time]
    return JsonResponse(response)


@ajax_required
@require_http_methods(["GET"])
def get_online_data(request, id):
    auction = Auction.objects.get(id=id)
    bids = auction.bids.all()
    bids_count = bids.count()

    if auction.closed_auction:
        if not bids_count:
            last_user_bid = auction.start_price
        else:
            last_user_bid = bids.filter(user=request.user).latest('time').price
        return JsonResponse({'count': bids_count, 'lastUserBid': last_user_bid})
    else:
        if not bids_count:
            return JsonResponse('No Bids', safe=False)
        bids_last = bids.first().price
        return JsonResponse({'count': bids_count, 'last': bids_last})


def charts_view(request, id):
    db_data = Auction.objects.get(id=id)
    if db_data.status not in ['2_S', '1_A']:
        return render(request, "error_pages/404.html")
    return render(request, "auction/auction_analysis.html", context={'card': db_data})


@login_required
@ajax_required
@require_http_methods(["GET"])
def favorite(request, id):
    auction = Auction.objects.get(id=id)
    user = request.user
    if user in auction.bookmarks.all():
        auction.bookmarks.remove(user)
    else:
        auction.bookmarks.add(user)
    return JsonResponse({"count": auction.bookmarks.count()}, status=200) # TODO шедулинг на смс


@login_required
@ajax_required
@require_http_methods(["GET"])
def bid(request, id, value):
    print(request, id, value)
    auction = Auction.objects.get(id=id)
    new_bid = Bid(user=request.user, price=value, step=value)
    try:
        new_bid.save()
        auction.bids.add(new_bid)
        auction.save()
        return JsonResponse({'code': 'success', 'message': 'Ставка принята'})
    except Exception:
        transaction.rollback()
        return JsonResponse({'code': 'error', 'message': 'Не удалось принять ставку' })



@login_required
@ajax_required
@require_http_methods(["GET"])
def get_auction_data(request, id, type):
    data = Auction.objects.get(id=id)
    document = settings.MEDIA_ROOT + '/' + data.doc_register.name
    data_dict = serialize_excel(document)
    if type == 'chart-details':
        osz_list = sorted([row['osz'] for row in data_dict])
        chart_data = [[ix, val] for ix, val in enumerate(osz_list)]
        return JsonResponse(chart_data, safe=False)
    elif type == 'chart-analysis':
        return JsonResponse({
            'sex': sorted([row['sex'] for row in data_dict]), 
            'age': sorted([row['age'] for row in data_dict]),
            'region': sorted([row['region'] for row in data_dict]),
            'osz': sorted([row['osz'] for row in data_dict]),
            'delay': sorted([row['delayPeriod'] for row in data_dict]),
            }, safe=False)
    elif type == 'datatable':
        return JsonResponse(data_dict, safe=False)
