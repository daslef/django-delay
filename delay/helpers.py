import requests
from random import randint

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponseBadRequest, JsonResponse
from django.views.generic import View

import pandas as pd
import pyexcel as pe
import django_excel as excel


def paginate_data(qs, page_size, page, paginated_type, **kwargs):
    """Helper function to turn many querysets into paginated results at
    dispose of our GraphQL API endpoint."""
    p = Paginator(qs, page_size)
    try:
        page_obj = p.page(page)

    except PageNotAnInteger:
        page_obj = p.page(1)

    except EmptyPage:
        page_obj = p.page(p.num_pages)

    return paginated_type(
        page=page_obj.number,
        pages=p.num_pages,
        has_next=page_obj.has_next(),
        has_prev=page_obj.has_previous(),
        objects=page_obj.object_list,
        **kwargs,
    )


def ajax_required(f):
    """Decorator to validate than a request is AJAX"""

    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()

        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


class AuthorRequiredMixin(View):
    """Mixin to validate than the loggedin user is the creator of the object
    to be edited or updated."""

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.user != self.request.user:
            raise PermissionDenied

        return super().dispatch(request, *args, **kwargs)


def is_owner(obj, username):
    """
    Checks if model instance belongs to a user
    Args:
        obj: A model instance
        username(str): User's username
    Returns:
        boolean: True is model instance belongs to user else False
    """
    return obj.user.username == username



def generate_code(length=5):
    return ''.join(str(randint(0,9)) for i in range(length))


def send_sms(data, phone):
    SMSC_URL = f'https://smsc.ru/sys/send.php?login={settings.SMSC_LOGIN}&psw={settings.SMSC_PASSWORD}&phones={phone}&mes={data}'
    r = requests.get(SMSC_URL)
    return r


def _process_excel(document):
    content = document.read()
    sheet = pe.get_sheet(file_type='xls', file_content=content)
    sheet.name_columns_by_row(0)
    sheet_dict = sheet.dict
    features = ["Общая сумма задолженности", "Сумма просроченного ОД", "Наименование регионального филиала", "Срок просрочки в днях"] 
    region_list, osz_list, od_list, delay_list = [v for k,v in sheet_dict.items() if k in features]

    osz = int(sum(osz_list))
    od = int(sum(od_list))
    average_debt = int(od / len(od_list))
    average_delay = int(sum(delay_list) / len(delay_list))
    multiregion = 'Да' if len([set(region_list)]) > 1 else 'Нет'
    region = ', '.join(set(region_list))
    n_cases = len(osz_list)

    return (osz, od, region, average_debt, average_delay, n_cases, multiregion)


def serialize_excel(document):
    raw = pd.read_excel(document)
    features = ['Дата рождения', 'Пол', 'Наименование регионального филиала', 'Сумма кредита', 'Дата выдачи кредита', 
            'Дата окончательного возврата кредита', 'Общая сумма задолженности', 'Сумма просроченного ОД', 'Дата выхода на просрочку', 
            'Срок просрочки в днях','Дата последнего платежа','Получен судебный акт?', 'получен ИД']
    raw = raw[features]
    raw.rename(columns={
        'Дата рождения': "age",
        'Пол': "sex",
        'Наименование регионального филиала': "region",
        'Сумма кредита': "sumOfLoan",
        "Дата выдачи кредита": "dateOfLoan",
        'Общая сумма задолженности': 'osz', 
        'Сумма просроченного ОД': 'od', 
        "Срок просрочки в днях": "delayPeriod",
        'Дата последнего платежа': "lastPayment",
        "Получен судебный акт?": "judgeAct",
        "получен ИД": "id",
        }, inplace=True)
    
    raw.iloc[:,0] = pd.Timestamp.now() - pd.to_datetime(raw.iloc[:,0])
    raw.iloc[:,0] = raw.iloc[:,0].apply(lambda x: x.days / 365)
    raw.iloc[:,0] = raw.iloc[:,0].astype(int)
    raw.iloc[:, 6] = raw.iloc[:, 6].astype(int)
    raw.iloc[:, 7] = raw.iloc[:, 7].astype(int)
    
    return raw.to_dict(orient="records")
