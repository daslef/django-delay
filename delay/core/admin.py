from django.contrib import admin
from delay.core.models import Auction, Bid


@admin.register(Auction)
class AuctionAdmin(admin.ModelAdmin):
    model = Auction
    list_filter = ("user", "status")
    list_display = ('id', 'user', 'allowed_company_types', 'count_bids', 'timestamp_start', 'timestamp_finish', 'status')

    def save_model(self, request, obj, form, change):
        # добавить верификацию перед аппрувом: должны быть установлены датавремя старта и завершения
        if 'status' in form.changed_data and form.cleaned_data['status'] == "A":
            print('HEHEY!')
            obj.schedule_reminder()

        super().save_model(request, obj, form, change)


@admin.register(Bid)
class BidAdmin(admin.ModelAdmin):
    model = Bid
    list_filter = ("user",)
    list_display = ('user', 'price', 'time', 'auction')