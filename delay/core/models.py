import uuid

import arrow
import redis

from django.conf import settings
from django.db import models
from django.contrib.auth import get_user_model

from slugify import slugify

from delay.users.models import User

class AuctionQuerySet(models.query.QuerySet):
    '''
    all - все подтвержденные админом лоты
    approved - подтвержденные админом лоты, которые еще не стартовали, участие в которых разрешено пользователю (за исключением продавца)
    drafts - заявки (лоты, еще не подтвержденные админом)
    started - текущие лоты, участие в которых разрешено пользователю (за исключением продавца)
    finished - завершенные лоты
    '''
    def get_approved(self):
        return self.filter(status="1_A")

    def get_drafts(self):
        return self.filter(status="0_D")

    def get_all(self):
        return self.exclude(status="0_D")

    def get_started(self):
        return self.filter(status="2_S")

    def get_finished(self):
        return self.filter(status="3_F")


class Auction(models.Model):

    # Добавить участников, добавивших в избранное
    DRAFT = "0_D"
    APPROVED = "1_A"
    STARTED = "2_S"
    FINISHED = "3_F"

    EFORM = "E"
    PAPERFORM = "P"

    STATUS = (
        (DRAFT, "На рассмотрении"), 
        (APPROVED, "Одобрен"),
        (STARTED, "Начался"),
        (FINISHED, "Завершен"),
    )

    user = models.ForeignKey(
        get_user_model(),
        null=True,
        on_delete=models.SET_NULL,
    )

    multiregion = models.BooleanField("Мультирегион", default=False)

    doc_register = models.FileField("Документ: реестр должников", upload_to=f"docs/auctions/", blank=True, null=True)
    doc_example_assignment_agreement = models.FileField("Документ: пример договора цессии", upload_to=f"docs/auctions/", blank=True, null=True)
    doc_impersonal_loan_agreement = models.FileField("Документ: пример обезличенного договора займа", upload_to=f"docs/auctions/", blank=True, null=True)
    doc_personal_account_statement = models.FileField("Документ: пример выписки по лицевому счету", upload_to=f"docs/auctions/", blank=True, null=True)

    osz = models.DecimalField("ОСЗ", max_digits=16, decimal_places=0, null=True, blank=True)
    od = models.DecimalField("ОД", max_digits=16, decimal_places=0, null=True, blank=True)
    region = models.CharField("Регион", max_length=40, default="", null=True, blank=True)
    average_debt = models.IntegerField("Средний долг", null=True, blank=True)
    average_delay = models.IntegerField("Средняя просрочка", null=True, blank=True)

    description = models.CharField("Пожелания", max_length=400, default="")
    start_price = models.DecimalField("Стартовая ставка", max_digits=17, decimal_places=0)
    minimal_price = models.DecimalField("Проходная цена", max_digits=17, decimal_places=0, null=True, blank=True)
    closed_auction = models.BooleanField("Закрытый аукцион", blank=True, default=False)
    
    accepted_mfo = models.BooleanField("Допуск: МФО", blank=True, default=False)
    accepted_banks = models.BooleanField("Допуск: Банки", blank=True, default=False)
    accepted_collection_agencies = models.BooleanField("Допуск: Коллекторские агенства", blank=True, default=True)

    timestamp_created = models.DateTimeField("Заявка подана", auto_now_add=True)
    timestamp_start = models.DateTimeField("Старт торгов", null=True, blank=True)
    timestamp_finish = models.DateTimeField("Завершение торгов", null=True, blank=True)

    status = models.CharField("Статус", max_length=3, choices=STATUS, default=DRAFT)
    has_winner = models.BooleanField("Есть победитель", default=False)

    bookmarks = models.ManyToManyField(User, related_name="auction_bookmarks", blank=True)
    bids = models.ManyToManyField('Bid', blank=True)

    objects = AuctionQuerySet.as_manager()

    class Meta:
        ordering = ("-timestamp_start",)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
    
    def __str__(self):
        return f'{self.user} - {self.id}'

    @property
    def is_current(self):
        return self.status == "2_S"

    @property
    def number_of_cases(self):
        return Case.objects.filter(auction=self).count()

    @property
    def allowed_company_types(self):
        r = []
        for fieldname in ['accepted_mfo', 'accepted_banks', 'accepted_collection_agencies']:
            if getattr(self, fieldname) is True:
                abbreviation = fieldname.replace('accepted_', '')[0].upper()
                r.append(abbreviation)
        return r

    def count_bids(self):
        return self.bids.all().count()

    def get_last_bid(self):
        last_bid = self.bids.last()
        return last_bid.price if last_bid else None

    def get_month(self):
        months = {1: 'Январь', 2: 'Февраль', 3: 'Март', 4: 'Апрель', 5: 'Май', 6: 'Июнь', 
                    7: 'Июль', 8: 'Август', 9: 'Сентябрь', 10: 'Октябрь', 11: 'Ноябрь', 12: 'Декабрь'}
        return months[self.timestamp_start.month]
        
    def schedule_reminder(self):
        from .tasks import send_sms_reminder

        if not self.timestamp_start:
            return

        auction_start_time = arrow.get(self.timestamp_start, "Europe/Moscow")
        reminder_time = auction_start_time.shift(minutes=-30)
        milli_to_wait = int(
            (reminder_time - arrow.now("Europe/Moscow")).total_seconds()) * 1000

        result = send_sms_reminder.send_with_options(
            args=(self.pk,),
            delay=milli_to_wait)

        print(f'Reminder was set on {reminder_time}!')
        return result.options['redis_message_id']


class Bid(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    price = models.IntegerField()
    step = models.IntegerField(default=25000) # ??
    time = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ["-time"]

    @property
    def auction(self):
        return self.auction_set.first()

    def __str__(self):
        return f'{self.user} - {self.price}'


# class Bookmark(models.Model): # TODO обдумать, переписать
#     auction = models.ForeignKey(Auction, on_delete=models.CASCADE)
#     user = models.ForeignKey(
#         get_user_model(),
#         null=True,
#         on_delete=models.SET_NULL,
#     )