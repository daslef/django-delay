from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "delay.core"
    verbose_name = "Core"
