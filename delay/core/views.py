from django.shortcuts import render


def error_400(request, exception):
    return render(request,'error_pages/400.html', status=400, context={})

def error_403(request, exception):
    return render(request,'error_pages/403.html', status=403, context={})

def error_404(request, exception, template_name='error_pages/404.html'):
    return render(request, template_name, locals())

def error_500(request):
    return render(request, 'error_pages/500.html', status=500, context={})
