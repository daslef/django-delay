import arrow
import dramatiq
from dramatiq.brokers.redis import RedisBroker

from django.conf import settings
from twilio.rest import Client

from .models import Auction


client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

dramatiq.set_broker(RedisBroker(host='redis', port=6379, db=0, decode_responses=True)) # перенести в сеттингсы

@dramatiq.actor
def send_sms_reminder(auction_id):
    try:
        auction = Auction.objects.get(pk=auction_id)
    except Auction.DoesNotExist:
        return

    auction_start_time = arrow.get(auction.timestamp_start, "Europe/Moscow")
    body = 'Hi {0}. You have an auction coming up at {1}.'.format(
        auction.user,
        auction_start_time.format('h:mm a')
    )

    client.messages.create(
        body=body,
        to='+79997185937', # TODO рассылать участникам, добавившим в избранное через аукцион.записавшиесяучастники.телефоны
        from_=settings.TWILIO_NUMBER,
    )