-r base.txt

boto3  # https://github.com/boto/boto3
gevent  # https://github.com/gevent/gevent
raven  # https://github.com/getsentry/raven-python
psycopg2 --no-binary psycopg2  # https://github.com/psycopg/psycopg2

django-storages  # https://github.com/jschneier/django-storages
