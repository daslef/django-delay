Delay - Online Auction of Debts
========

Technology Stack
----------------

- Python: https://www.python.org/
- Django Web Framework: https://www.djangoproject.com/
- PostgreSQL: https://www.postgresql.org/
- Redis: https://redis.io/documentation
- Daphne: https://github.com/django/daphne/
- Caddy: https://caddyserver.com/docs
- Docker: https://docs.docker.com/
- docker-compose: https://docs.docker.com/compose/
- WhiteNoise: http://whitenoise.evans.io/en/stable/
- Django-channels: https://channels.readthedocs.io/en/latest/
- Sentry: https://docs.sentry.io/
- Cookiecutter: http://cookiecutter-django.readthedocs.io/en/latest/index.html
