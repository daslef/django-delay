from django.conf import settings
from django.conf.urls import include, url, handler400, handler403, handler404, handler500

from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from django.urls import path


handler404 = 'delay.core.views.error_404'
handler500 = 'delay.core.views.error_500'
handler403 = 'delay.core.views.error_403'
handler400 = 'delay.core.views.error_400'

urlpatterns = [
    url(r"^$", TemplateView.as_view(template_name="index.html"), name="index"),
    url(r"^grappelli/", include('grappelli.urls')),
    url(settings.ADMIN_URL, admin.site.urls),
    url(r"^users/", include("delay.users.urls", namespace="users")),
    url(r"^seller/", include("delay.seller.urls", namespace="seller")),
    url(r"^lots/", include("delay.lots.urls", namespace="lots")),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += [
        url(
            r"^400/$",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        url(
            r"^403/$",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        url(
            r"^404/$",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        url(r"^500/$", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [url(r"^__debug__/", include(debug_toolbar.urls))] + urlpatterns
