from django.conf.urls import url
from django.urls import re_path

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator

from delay.lots.consumer import BiddingConsumer

application = ProtocolTypeRouter(
    {
        "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(
                URLRouter(
                    [
                        re_path(r'ws/(?P<room_name>\w+)/(?P<user_role>\w+)/$', BiddingConsumer.as_asgi()),
                    ]
                )
            )
        )
    }
)
