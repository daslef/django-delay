import logging
import socket
import os

from django.utils.log import DEFAULT_LOGGING

from .base import *
from .base import env


DEBUG = env.bool("DEBUG")

SECRET_KEY = env(
    "SECRET_KEY",
    default="fOqtAorZrVqWYbuMPOcZnTzw2D5bKeHGpXUwCaNBnvFUmO1njCQZGz05x1BhDG0E",
)

ALLOWED_HOSTS = ["localhost", "0.0.0.0", "127.0.0.1", "172.20.0.1"]

AUTH_PASSWORD_VALIDATORS = []


FIXTURE_DIRS = [
    str(APPS_DIR.path("fixtures"))
]

TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG


# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INSTALLED_APPS += ["debug_toolbar"]
MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]
DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}
INTERNAL_IPS = ["127.0.0.1", "10.0.2.2"]


if os.environ.get("USE_DOCKER") == "yes":
    hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
    INTERNAL_IPS += [ip[:-1] + "1" for ip in ips]

# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ["django_extensions"]
